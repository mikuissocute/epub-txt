カット＆ペーストでこの世界を生きていく
咲夜
http://ncode.syosetu.com/n8366dn/

在這個世界，成年時會從神大人那裡得到名為《技能（スキル｜Skill）》的技能。
主角被授與了兩種《技能》，以此為契機成為了被稱為英雄的存在。

這樣平凡的故事。

成人を迎えると神様からスキルと呼ばれる技能を得られる世界。
主人公は二つのスキルを授かり、それをきっかけに英雄と呼ばれる存在へと成り上がる。

そんなありきたりの物語。
…………………………………………………………………
★★　ツギクルブックス様より2017年6月10日書籍第1巻が発売となりました！　★★

　　　　2018年1月10日に書籍第2巻が発売されました。　　　　　　　　★★

　　　　2018年9月10日に書籍第3巻が発売予定となっております。　　　　　　　★★

★★　また2018年5月30日からコミカライズも始まっております。
★★
★★　　　これもお読み頂き応援を頂いております皆様のおかげです！本当にありがとうございます　　　　　　 ★★


更新時間は朝6:00となります(予約投稿)※現在病気療養中につき、不定期更新になっています。申し訳有りません。

小説自体は書いておりましたが、投稿自体は本作が初となります。
作法や文体などお見苦しい部分もあるかと思いますが、生暖かい目で見て頂ければ幸いです。

なお、本作内では
・単位表記
・諺(ことわざ)
・動物類の種別等
これらの物や他単語などについて読者の皆様が理解しやすいように日本で使われている物を
意図的に使っております。

…………………………………………………………………
★皆さまのおかげで「71,588,278PV」「総合評価115,331pt」達成致しました！★

拙い文章ではありますがお読み頂いた皆さまに心からお礼申し上げます。
本当にありがとうございました。

－－－－－－－－－－－－－－－
其他名稱：
以複製貼上在這個世界生活
以剪切粘贴在这个世界生活

－－－－－－－－－－－－－－－
貢獻者：Nodius、ScarletFlash、smallpiggod、懶惰的咸魚、我只是个路人、狗管理

標籤：R15、node-novel、syosetu、アビリティ、エルフ、クラン、スキル、チート、ファンタジー、一夫多妻(ハーレム)、人によって鬱展開有、冒険、姫騎士、残酷な描写あり、異世界、神獣、美人受付嬢、魔法

－－－－－－－－－－－－－－－
目錄索引：
- null
- - 第01話 技能授與
- - 第02話 使用了技能
- - 第03話 組合的奇跡
- - 第04話 今後の方針
- - 第05話 能力と初戦闘
- - 第06話 オークの買い取り
- - 第07話 オークよ、再び
- - 第08話 錬金術と謎の男達
- - 第09話 冒険者公會的約束
－－－－－－－－－－－－－－－

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝CHECK
第00001話：null
null
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

－－－－－－－－－－－－－－－BEGIN
第00002話：第01話 技能授與
第01話 技能授與
－－－－－－－－－－－－－－－BODY

我的名字是馬恩（マイン｜Myne）。今天迎來了成年的１５歳，這樣就加入了大人的行列了。

迎來成年的孩子們前往神殿，就能被神大人授與最多三個名為技能的能力。
技能中有戰鬥向的和生活向的東西，也有使用方法完全不明的東西。

而且，這些技能根據被授與的組合，也可能化為意想不到的能力。

過去被稱為英雄的人們都因技能的組合而獲得了成就。

例如，被授與兩個技能的国王大人──【單手劍・聖】和【腕力強化・大】的組合。

以【單手劍】為代表，戰鬥系技能在強度上被分為數階級。
現在已知的是【單手劍】→【單手劍・極】→【單手劍・聖】→【單手劍・神】等四階級。

換言之，国王大人獲得了階級裡第二強的【單手劍】能力。

然後，還有【腕力強化】的技能。
正如字面所述是強化腕力的技能，與剛才的【單手劍】技能的兼容性非常高。

【腕力強化】在強度上也被分為數階級，在国王大人的情況下也是階級裡第二強的技能。

国王大人由於這個組合得到了無雙之力，並在那之後留下了諸多功績而成為了国王。

神殿的見解似乎是──技能持有的效果越強，被授與的概率就越低。
因此，持有強大效果的技能以最適當的組合被授與的事少之又少。

不管怎麼說，技能的個數為無限⋯⋯雖不能這麼說，但至少數之不盡。
其中效果強大的技能則為數不多。

正因如此，国王大人所獲得的技能組合尤其突出。

迎來成年的孩子們及其家族們，滿懷希望地步向了神殿。
對，夢想著如同国王大人那樣「中獎」

順帶一提，雙親早在我１０歳時因流行病逝去了。

父親是名優秀的獵人。

設法模仿父親，用遺留在家的陷阱狩獵了野兔等小動物。
此外，通過採集藥草等手段獲得了每日的食糧。

不管怎麼說，未成年人並沒有技能，因此狩獵的獵物只有日常生活的量。
但由於父母的交際良好，附近的肉店先生和錬金術先生以稍高的價格收購了。
雖然是勉強的生活，但是時至今日都是一個人活下去的。

但是，這麼艱辛的生活在今天也要結束了……應該吧。
只要得到的技能還過得去的話，就能過上比現在更輕鬆的生活。

根據技能的組合，也可能一舉成名。

我與他人同樣，懷抱著希望和夢想步向神殿。


◆◇◆◇◆◇◆◇◆◇◆◇

到達神殿後，在期待與不安的交錯下，帶著不可言喻的高掦感一起等待了４０分鐘。

其間，每當看見得到技能的人那些微妙的表情，就想著「難道沒有得到好技能嗎」，令不安的情緒越來越強。

「那麼，輪到你了。」

在那時候，終於輪到我了。

「請進入那個圈中，懷著對神大人的感謝之情為能長大成人的事祈禱吧。」

我依指示進入了圈內，並向神大人獻上了祈禱。
雖然雙親已逝，但還是為能夠迎來今天的事深表謝意。

於是，我的身體隱約地閃耀了數秒，然後腦海中浮現了兩個技能。

「嗯，似乎順利地被授與技能了。請把手放在這顆珠子上。」

正如所說的那樣，把手搭上了伸出的透明珠子。
然後，珠子中顯示了剛才浮現在腦海中的兩個技能──【鑑定・全】和【剪切與黏貼（カット＆ペースト｜Cut And Paste）】。

名為【鑑定】的技能可調查萬物的詳情，應該是相當稀有的技能。
經常聽聞的是【鑑定・年齡】、【鑑定・名稱】、【鑑定・動物】、【鑑定・性別】……等。
即使是稀有技能，【鑑定】之中也有差別，那就是只能調查附於「鑑定」後的屬性。

例如性別之類不特意【鑑定】也能普通地分別，因此即使同為【鑑定】也可視為失敗。

在這種情況下，我被授與的【鑑定】其後是「全」

也許這就是【鑑定】中最高級的屬性。
要是正如描述的話，那麼能調查的事物就沒有限制了。

雖然還沒想到要如何活用這個，但是可以認為是成功了。

然後，此外還有【剪切與黏貼】，說實話這個相當微妙。
從沒聽說過名為【剪切與黏貼】的技能本身，卻聽說過【切斷（カット｜Cut）】和【黏合（ペースト｜Paste）】。

【切斷】是把所見之物「切斷」的技能。

例如在烹飪時沒有廚具的情況下用來切食材，或是處理不能輕易以刀具通過的那些堅硬或柔軟的事物，可說是非常便利的技能。

但是，這個技能的對象基本上僅限於靜物而非生物。
如果對生物也有效的話，在對魔物戰中就會是非常有利的技能吧。

不過把魔物打倒後，解體時是相當便利的技能。
解體作業需要花費大量的功夫和時間，而血的氣味隨著時間的流逝有可能引來新的魔物。
由於這能夠大幅地縮短解體作業的時間，因此對冒険者們而言是非常受歡迎的技能。

【黏合】是把所見之物在任何地方「黏合」的技能。

就是可以作為黏合劑使用，但不同的是一旦黏合了就連本人也無法剝掉。
假如誤把手指和素材黏合在一起，那就變成一個荒唐的結局了。

若是使用前述的【切斷】，或許能夠剝掉（正確來說是「切斷」而非「剝掉」）。

可是，一般沒有被同時授與【切斷】與【黏合】的人，因此基本上可說是易用性非常差的技能。

然而，聽說被稱為工匠的生產者能夠精準無比地使用這個技能，並製作出結實的道具和家具。
因此，根據熟練度的情況也可以是實用的技能。

順帶一提，【黏合】似乎能以生物為對象來使用。

世間上除了少數的例外，持有者都會有「這兩種技能一起時挺方便！」這樣對技能的認知。
在我的情況下，由於【剪切與黏貼】能抵消【黏合】難用性，因此比單獨授與的人們更方便使用。

「嗯，授與的【鑑定・全】似乎是極好的技能，而【剪切與黏貼】根據使用方法也挺便利吧。」

神殿的人窺視著透明珠子並向我搭話了。

「那麼今天的日程結束了，願你今後過上幸福的人生。」

我向神殿的人道謝後，踏上了歸家之途。

－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00003話：第02話 使用了技能
第02話 使用了技能
－－－－－－－－－－－－－－－BODY

一边考虑着被授予的技能的事，我踏上了回家的途中。
乘坐的馬車上坐着十名乘客。

我家位于有神殿的王都半日馬車距离的盧卡斯城镇。

是距离王都比较近的城镇，人口在１０００人左右，是以冒険者为首的人往来频繁的城镇。

「那么，试着用用鑑定看吧。」

在馬車中摇晃着，對面前的壮年冒険者默念鑑定投以視線。
于是腦海中浮现出了情报。

姓名：基斯
種族：人族
性別：男
年龄：４８歳
職業：冒険者（C級）

【技能】

單手劍
清扫

嘿，是基斯先生吗。
持有的技能是……【單手劍】和【清扫】吗……

组合不好的典型呢。

話說，这幅容貌和【清扫】的差距也太大了。

一边考虑着清扫的事，继续凝視着基斯先生的时候，腦海中继续浮现出了情报。

【清扫】：能够有效率地扫除，熟练度提高的話，能更干净地打扫。

是吗，鑑定还可以對确认到的情报项目更详細地調查吗。
这个很方便啊。

大概，因为是【鑑定・全】才做得到这種事吧。

然后，剛才得到的情报中包含了很令人在意的词。

「熟练度」哒。

虽然哪里都看不到熟练度这样的项目，从这句話看来技能是越使用就越能提高能力的吧。

原来如此，这样的話越是使用鑑定，就能取得越详細的情报吧。

这样的話，就不断地鑑定吧。
反正，半天中都不能从馬車中离开。

【剪切＆粘贴】在有人的地方不好试验。

一边考虑着那样的事，對馬車中的１０全部展开鑑定。

９人都没什么問题……
但是，鑑定第十人的男性取得了不妙的情报。

姓名：蓋斯卡爾特
種族：人族
性別：男
年龄：３１歳
職業：盜贼

【技能】

裁缝
短劍・极
俊足（小）

竟然，是盜贼哒！
为什么会在合乘馬車上哒！？

而且还有着【短劍・极】。

也有相应的手腕吧。

不妙，不妙哦。现在虽然老實地闭目坐着，不知什么时候就会展开暴行。

虽然有數名冒険者称作，被突袭砍来的話也会讶異死掉吧。

那家伙的目的是什么？
这个馬車的目的地是我居住的盧卡斯城镇哒。

在馬車中很老實，是瞄准了盧卡斯城镇吗。

进入城镇会受到严格的检查，所以盜贼进不去的。

不管怎样思考都想不到正解，不，想了也没有。

现在，知道那家伙很危險的只有我。
即便對谁說，在狭小的馬車中說那種事當然会被那家伙听到。

有什么，有什么办法吗。

在那家伙拿出短劍，暴起之前有什么办法……

即便很焦虑，但也没有我这種剛成年的青年能做的事。
是这样认为的。

最好是那家伙没有暴起，在抵達城镇后趁着他不注意跑回家。
但是，完全不知道那家伙是否会就这样老實着。

即便平安抵達了城镇，也不知道会不会在城镇中引发什么，也没有我不会被卷入的保証。

最重要的是馬車中有和我一样剛迎来成人的青年，是年轻的女性。
果然不考虑什么解決方案的話，肯定会遇到危險的吧。

我现在能做些什么？

說道能做的事，只有今天剛获得的技能了。

鑑定确實派上了用場。
但是，无法推翻现在这个状况。

那么，另一个技能【剪切＆粘贴】能不能好好活用呢。

【剪切】不能用于生物。
即便用出来了，也会变成對什么都没做的對手展开了攻击。

那样的話被捕的不是那家伙而是我了。

不给對手危害，无力化的話……

對了，用【粘贴】把那家伙的靴子粘在地板上。
这样的話，突然想要行动也会无法动弹。

即便想脱掉靴子，也没法立刻做到吧。

之后是挂在腰間的短劍。
把鞘和本体粘上吧。

这样的話，即便发出什么行动，基斯先生们冒険者也能进行對应哒。

好，決定了就尽快行动。

发动了技能，好好发动了的手感传回腦中。
虽然是初次使用，好像能用。

呼唔……这样就暂且安心哒。

但是，就这样抵達城镇无法後續對应。

靴子粘住了，脱掉就能移动了。
短劍重買新的就好了。

不知道那家伙的目的是这馬車，还是盧卡斯城镇中。无法根本上解決。

就这么堂堂正正乘在馬車上。
如果，是打算进入盧卡斯城镇的話，目的还在那之后吧。

这样的話，城镇里可能引发什么被害。

防住了面前的危机，我稍微冷静了一点。

然后，冷静下来的我……想起了没什么所谓的事。

－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00004話：第03話 組合的奇跡
第03話 組合的奇跡
－－－－－－－－－－－－－－－BODY

然后，冷静下来的我，想起了一件小事。

组合技能，能大幅提高能力。

像国王那样高能力的劍術+腕力強化一样，提高單体的效果，成为更強大的技能。

說不定，我所得到的两个技能也可能是具有相乘效果的组合。

我这样想着，對于可能的效果感到了期待和恐惧，试着进行實践。

首先，鑑定蓋斯卡爾特。

姓名：蓋斯卡爾特
種族：人族
性別：男
年龄：３１歳
職業：盜贼

【技能】

裁缝
短劍・极
俊足（小）

當然，和剛才鑑定的内容没有什么变化。

然后，在这里我使用【剪切】。

對象是蓋斯卡爾特所持有的技能【短劍・极】哒。

于是，腦内和剛才的粘贴一样，传来了剪切发动了的手感。

然后，再一次鑑定蓋斯卡爾特。

姓名：蓋斯卡爾特
種族：人族
性別：男
年龄：３１歳
職業：盜贼

【技能】

裁缝
俊足（小）

！！！！

正如所料，技能切割掉了！

【剪切】是把眼睛看到的東西剪下来的技能。

用【鑑定・全】，是能用眼睛（正确地說是在腦内）看出對象的情报。

然后，剪下来的技能是………
用粘贴粘在我的技能栏里。

姓名：瑪因
種族：人族
性別：男
年龄：１５歳
職業：猎人見习

【技能】

鑑定・全
剪切＆粘贴
短劍・极

！！！！！！！！

……做到了！做出来了！！

这種事，人只能由神明赐予才能得到技能，但我得到了其他入手的手段。

在自己身上发生的这件事，让我感到无比的冲击。
真的可以吗，我得到了这样的力量。

从某種意義上說，比知道蓋斯卡爾特的真面目时受到了更大的冲击。

但是，动摇也隨着时間的逐渐变小，渐渐地冷静起来。

回到家的話，必须得考虑以后怎么走呢………

首先，从蓋斯卡爾特那里剪下剩下的技能。贴在我的技能栏里吧。

姓名：蓋斯卡爾特
種族：人族
性別：男
年龄：３１歳
職業：盜贼

【技能】

无

好，这样就行了。
即使这家伙的目的在盧卡斯城镇里，也不用担心了。

……大概。

我能做的就到此为止吧。

不，在城镇看了他们的动向之后，跟門衛商量一下吧。
这样的話，門衛应该会处理的。

总而言之，對于那家伙的应對達成了目标，全身都放松了紧张感。

另外，也很有可能在馬車中暴行起来，不能疏忽大意，但还是稍微放松身体，轻松一下。


◆◇◆◇◆◇◆◇◆◇◆◇

从王都出发，差不多已经过了五个小时了吧。
终于发生了担忧的事。

恐怕是蓋斯卡爾特的伙伴们吧。

突然发现了冲向这辆馬車的武装集團。

在馬車内的人们看到外面的那一瞬間，他气势满满地动了。

「唏哈，老實点……哇！」

……但是，因为事先准備好了，鞋子在地板上粘着，气势汹汹地站起来时就这样豪爽地倒在了地上。

虽然是突然的事，但馬上被在車内的冒険者们按住捆好了。

然后，護衛和冒険者们汇合，調整态势迎击前来的盜贼集團。

「我们也能借出力量，御者啊，當然报酬也会稍微拿出一点吧？」

「是的，當然了」

听着这样的對話，我赶紧對过来的集團进行鑑定。
集團全部是八人，特別在意的是这三个人。

姓名：拉芙蕾
種族：人族
性別：女
年龄：２６歳
職業：盜贼

【技能】

双手劍劍
視力強化・中

姓名：阿依恩
種族：人族
性別：男
年龄：３０歳
職業：盜贼

【技能】

礼法作法
脚力強化・小

姓名：休布
種族：人族
性別：男
年龄：４２歳
職業：盜贼

【技能】

魔法・風

剩下的五个人没有對戰斗的技能，不必視为威胁。

总而言之，使用魔法的那家伙很危險。

立即使用剪切无力化，贴到我的技能里。

剩下的另外两人，迅速剪切掉技能，并粘贴到我的技能里。
……但是，明明是盜贼，却有礼仪作法………

最开始注意到不能使用技能的，是名叫休布的盜贼。

是打算从远距离使用魔法的吧。

即使好几次想用，也一直没魔法，視線动摇了。

接下来，注意到的是名叫拉芙蕾的女盜贼。
視力突然下降了吧。

好几次擦着眼睛的样子。

看着那样子的时候，距离渐渐缩短，盜贼们和冒険者们进入了戰斗中。

與无法使用技能而无法戰斗的盜贼们數量占優，但戰斗经验丰富的冒険者们轻松击败了。

其實是打算让蓋斯卡爾特从内部搅乱，趁着空隙用包含魔法的攻击来压制这边的吧。

大前提被全部推翻了的盜贼们一个两个地，被打倒了。

从戰斗开始１０分钟后，包括蓋斯卡爾特在内的九名盜贼中两人死亡，七人被捕，我们的馬車确保了平安。

－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00005話：第04話 今後の方針
第04話 今後の方針
－－－－－－－－－－－－－－－BODY

解決了盜贼的袭击，终于回到了镇上。

被冒険者抓住的盜贼们作为犯罪奴隷，将被出售。
据說出售的货款包含协助迎击的乘客冒険者们，会均等支付。

我也给予了帮助，但因为不知道告诉別人【剪切＆粘贴】到底好不好，所以決定不說了。

「噢！瑪因回来了，顺利获得技能了吗？」

熟悉的門衛，埃德加看到我打招呼。

「是的，能做点什么了。」

「是吗，那太好了。」

浮现出亲切的笑容，談了談就回到了門衛的工作。

姓名：埃德加・曼賽爾
種族：人族
性別：男
年龄：２４歳
職業：守衛

【技能】

双手槍・聖
礼仪作法
铁壁

呜哇，埃德加很厉害啊。

有着【双手槍・聖】。

而且，铁壁也像是強大的技能。

話說，铁壁是什么技能呢？

再次鑑定铁壁。

【铁壁】：任意发动型技能，可以在自己期望的时机发动。在发动中防御力上升三倍。使用一次的話，到下次使用需要３０分钟的冷却时間。

嘿，防御力三倍，愛德格是攻守兼備，很厉害的人啊。
冷却时間……使用一次的話，会有无法使用的时間。

也有这样的技能，记住吧。

顺便說一下虽然是理所當然的事，但理所當然地不会剪切埃德加先生的技能。
決定了使用的對象只有敌對的人和盜贼之类的壊家伙。

如果一个劲地剥来的話，和強盜什么一样吧。

和埃德加道別后，馬上就进入镇上了。

一边隨意地进行鑑定，我走到自己的家里。
发现了各種各样自己不知道的技能很有意思。


◆◇◇◇◇◇◇◇◇◇

我的家在镇外。

虽然现在只住了我，原本是和爸爸媽媽一起住的家，虽然不能說是豪宅，但我覺得是个挺不错的家。

「我回来了」

虽然谁都不会回复，但还是会不知不覺地說出口来。

走进了家里放松下来，我扑进了床上。

「呼呜……总覺得累了啊。」

暂且，发着呆休息吧。

怎么說呢，动荡的一天啊………

啊，對了，得做自己的鑑定吗。

姓名：瑪因
種族：人族
性別：男
年龄：１５歳
職業：猎人見习

【技能】

鑑定・全
剪切＆粘贴
短劍・极
俊足（小）

裁缝
魔法・風
双手劍
視力強化・中
礼仪作法
脚力強化・小
料理
交涉術
錬金術

在戰斗中剪切的三名盜贼以外，也从其他的家伙那里隨意剪切来了用得上的技能。
料理、交涉術、錬金術这三種。

考虑今后的方针，认为是能派上用場的技能。

但是，今天一天就一下子增加了技能啊。
虽然现在还好，但是隨着今后的增加，鑑定的結果也会变得很难读了………

虽然按照取得的顺序排列着技能，但是这个【剪切＆粘贴】不能調整吗。

……結果，可以移动技能的位置。

【技能】

鑑定・全
剪切＆粘贴
短劍・极
双手劍

脚力強化・小
視力強化・中

俊足（小）

魔法・風

料理
裁缝
礼仪作法
交涉術
錬金術

嗯，这样稍微容易看了吧。今后定期調整吧。

……那么，考虑一下今后的事吧。

首先用【剪切＆粘贴】把技能剪掉变成自己的東西这一事實。
能不能告诉其他的人呢………

我认为这个选择是非常重要的事情。

无论我怎么在心中自己決定，即使說了決心只从敌對的對手身上剪下技能，也无法相信吧。

这样也是當然的，我也会避开的。

那个，就是这样吧。
如果不注意的話，可能会被偷走自己的技能，想想就可怕不想靠近。

变成那样的事的話，就会妨碍生活。

不和別人扯上關系而活下去什么的做不到啊。

这样的話，果然这件事还是保密吧。

接下来是關于今后的生活。

多虧了父亲留下的陷阱和猎人道具，至今为止也没挨饿，但是既然得到了技能，就想活用这个。

那么，该做些什么……但是………

活用【剪切】的生意……最可靠的是被冒険者雇用去解体的解体店吧？

但是，冒険者有各種各样的性格的家伙，解体了不肯付錢的人也会出现吧。
有过一次的話，也许一直都拿不到錢了。

可能是担心过頭了，但是感覺可能性很高。
姑且列入候补，总之先保留。

【粘贴】怎么样？

不，只想到了工匠……多少比剪切好一点，但在獨當一面之前太花时間了。

驳回，驳回！！

只使用神赐予的技能，意外地很难。

－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00006話：第05話 能力と初戦闘
第05話 能力と初戦闘
－－－－－－－－－－－－－－－BODY

結果要是說用【剪與贴】来做什么的話还挺难的啊。

解体屋……等等?好不容易才增加了技能，试试作为猎 人，好好行动吗？

【短劍（極）】【俊足（小）】【視力強化（中）】【脚力強化（小）】，如果能运用这类技能的話，不仅兔子这種小动物，說不定还能猎 殺大型动物。

嗯，试试往那个方向努力吧，这样的話即使增加（偷）了新技能，附近除了我以外也没有其他人应该不太会暴露吧……

事不宜迟，明天就出去狩 猎吧。

虽然少有粗略，既然已经決定方向了，吃了晚餐就快洗洗睡吧

我一边想着那样的事情，一边准備着像往常一样朴素的晚餐。

因为今天没有时間去買東西，所以只有干肉和黑面包。

因为量也没有多少，简單就吃完了，但是感覺比平时好吃。大概是【料理】发动了吧？

我想刷料理熟练度啊。


◆◇◆◇◆◇◆◇◆◇◆◇

早晨，當周边还是一片黑♂暗的时候我就醒来，迅速开始做狩 猎准備

装備上皮革做的胸甲，皮革的手甲，皮革的腿甲，把解体用的短劍在腰間插好。

如果是在平常，会做陷阱抓像兔子之类的小动物，不过，今天的目标是森林羊（？）【フォレストマトン】，所谓的羊。

而且速度虽然不是很快，却会用尖锐的的角来进行突击，还会用睡眠魔法，超鸡儿麻烦。至今为止都是看到了掉頭就溜的魔物。

But，就是因为是很麻烦的鸡儿玩意，所以賣得很貴。

肉虽然有些骚味，却很美味，角和蹄子作为素材可以以高價賣出。

如果能安稳的狩 猎这个的話，生活方面就会轻松不少。

准備也完成了，說来也是第一次正式的狩 猎，有点激动，之后从街道后門向森林附近去。

門衛桑說道「今儿是真吉儿早啊，还是小心点好Yo」被这样担心了。

在向森林行进的路途中我有了获得新技能后的實感，没错【俊足（小）】だ。

只是轻松的慢跑，就達到了至今为止要全力才能達到的速度。并且完全没有喘气。

再次体会到了技能这玩意儿的有用性和效果。

拜此所赐，一眨眼的功夫就到達了目的地。

「那么（さ～て更有画面感），这里开始就是重頭戏了。」

也许是因为紧张，腦内所想不由自主的脱口而出。

我慎重的走进森林。

名字：森林兔叽
種族：兔族
性別：雌性

【技能】

无

虽然不是目标的羊，不过，发现了一直承蒙它關照的兔叽先生。

嗯？怎么，出现了不熟悉的鑑定結果啊。

能力？？？这啥玩意儿？

不明白的東西要交给鑑定来处理吗。

【技能】：根据怪物体格與构造的不同而存在的特殊本領，被視为一般性技能（Ability）

……怪物的技能？說明描述了怪物专用技能的事。

以体格與构造而言，即使剪了也說不定是人族无法使用的。

总之，與一直用陷 阱 捕 获的對手，正面對剛试试，确认自己的力量吧。

用手拔出短 劍，站到了森林兔叽的正面。

注意到了我的森林兔叽愤怒地向我快速冲来。

发动【脚力強化（小）】的同时，闪身躲开兔叽的攻击，过程中快速刺 出 短 劍。

【叽！～～】

感覺到肉被切 裂开的感覺和听到森林兔叽的悲鸣后，看到了卧在地上流 血被面的兔叽。

还有一口气的样子，看起来受了很重的伤在地上piapia地抽 搐。

我挥舞着短 劍向上，倒握着它，就这样一口气刺 进了它的心 脏。

「哦吼！打倒了！」

對用非常干净利落的动作打倒兔子的事實感到惊讶的同时，我用【剪切】将兔子解 体了。
然后放入了隨身携带的收納袋里。

这是父亲遺留下来的可以放入50kg（單位都不改，这么懒的吗）左右的物品的收納袋。
不能放入活的東西，就这样将猎物放入后，忘记了的話，放在里面会腐烂。是珍貴的道具。

那么（さて）来找压轴的對手──森林羊吧。


◆◇◆◇◆◇◆◇◆◇◆◇

过了２０分钟左右，我在森林里兜圈子的同时又打到了两只森林兔，可就是找不到重要的羊。

在思索着原因的时候，余光瞟到的黑色大身影引起了我的注意。

用【視力強化中】来看的話…哦吼，哥布林大鬼（？）（オーク）

說到大鬼，特別就是說魔族中拥有着人形的一種怪物。
拥有殘暴的腕力，坚硬的皮肤，是相當麻烦的家伙。

而且，他们因为会把人类女性抢走，當做繁 殖工具而有名。（咕殺？）

因此，镇上的居民非常嫌惡它们。

港真，我覺得我的胜率很低。
以我的腕力来讲，我应该不能把短劍刺入它们坚硬的皮肤。

总之，先鑑定看看。

名前：豬人
種族：魔族
性別：雄性

【技能】

豪腕

【能力】

咆哮

む、技能和能力两者都持有吗。
是该說和兔子规格不在一个层面吗。（高玩）

总之，先抢技能吧。

能力就抢了贴在那边的樹上吧，
人类好像用不了。（这么隨便）

【豪腕】：任意发动型（主动？）技能，可以在自己想要的时机用出来使用，之后力量会变成两倍。使用过过后再次发动需要３０分钟时間。

……这样的話总有办法打到了吧？
技能和能力被夺走后就是个弱子♂，感覺我所担忧的坚硬皮肤只要有豪腕就总会有办法的。

但是正面硬剛不是上策呢。

哦哦哦哦哦哦哦，對哦，我还有魔法来着！（挂Bi大胜利）。因为完全没用过所以给我忘了。

背后陰它一发魔法后来戰吧。

这么決定了后，就绕到他后面。

『Surprise！Mf！』

再发射心中呼喊的【魔法・風】

風魔法切裂了挡在豬人前面的樹木后，再次向豬人的方向飞去。

〈嘎？〉

或许是注意到切裂木頭的声音，豬人轉頭向了这边。
但是，或许是被突发而来的状况打了个措手不及，風魔法漂亮的给它来了一发。

〈咕噢噢噢噢噢噢噢噢哦哦哦！！〉

就是现在！

发动【脚力強化・小】和【俊足（小）】之后一口气冲进它的下怀，
然后，用从那家伙那里夺过来的【豪腕】打爆它的肚子。

被痛苦缠绕着的豬人，胡乱挥舞着它的手臂。
另一边，我向后退去，再次发动【魔法・風】

极近距离下，豬人被重击的腹部又被風魔法来了一发，动作以肉眼可見的速度慢了下来。

在空隙間，我绕到那家伙的后面，不及思索的吧短 劍刺入它的后颈。

似乎到这就結束了，那家伙仰天大叫一声，倒在了地上。

打倒了？？？吗？（作者这么打的）

暂时也没有看見它再动。

用【剪切】来解体的話，對活物不能使用，所以可以判定生 死。

解体后，充分証明了他已经被打倒的事實。呼，真紧张啊。

但是，打倒豬人了！我打倒豬人了！

－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00007話：第06話 オークの買い取り
第06話 オークの買い取り
－－－－－－－－－－－－－－－BODY

能夠擊敗意想不到的敵人，使我開始能夠對戰鬥有一點信心。並再次體驗到所謂技能的有用性。

「到目前為止，我只憑藉著昨天所獲得的技能……如果我獲得更多技能會怎樣？」

不知不覺中，一句話從口中冒出。我還發現了怪物中存在技能和能力的差別。當然，是不可能從鎮上的人那裡獲得技能的，但如果以怪物為對象的話，你就不必手下留情了。充分的使用剪切技能削弱怪物，再利用該技能，擊敗它。
接下來，讓我一邊解體一邊將它放入袋中。不斷的使用著，大鬼逐漸的被解體。一般被認為體重超過１００公斤的大鬼並不能全部放入袋中，因為容量完全不夠。我決定拿掉我先前擊倒兔子所得到素材，並篩選大鬼那些能夠換成錢的部分，並將其放入儲物袋中。

「……一定要買一個容量更大的包」

趕緊把大鬼的素材放到袋中，接著回到城鎮。這裡和也起了很大的作用。
雖然我比平常更深入森林的地方，但
在那種情況下，也許我應該成為冒険者而不是解體屋。如果你繼續當解體屋的話，你遇到的怪物類型也是不變的。當然，會失去很多學習技能的機會。那，要是成為冒険者會怎樣？恐怕會為了達成委託而和各式各樣的怪物戰鬥。這樣的話，我就必須擁有一定強度。「成为解體屋」這個昨天的決定，由于打倒大鬼這個強敵而開始有所動搖。

「總之，優先進行大鬼的解體吧。不快點做的話，就會有其他怪物被血腥味吸引而來了。」

靠著和的幫助下，很快的就出了森林。在向熟悉的門衛打過招呼後並出示居民證後，進入了城鎮，接著便向熟悉的屠戶搭話了。

「嗨，叔叔你好」
「哦，是瑪因啊，你帶著一隻兔子回來呢！」

老闆笑著說

「事實上，今天真是太棒了。」

我在攤販上，放下了大鬼的肉。

「……小子……這個是……大鬼的肉？」

他似乎對此感到驚訝，我則是有些興
奮。

「是的沒錯，我設法利用昨天在神殿獲得的技能狩獵的！」
「哇…你也是個成年人了呢。能夠擊敗大鬼，被授予了……相當厲害的技能啊，不得不對神表示感謝啊」

順帶一提，在這個世界上，不去打聽別人的技能是種禮貌。如果是在眼前是用易懂的技能就算了，而在這里装做沒注意到是不成文規定。雖然有時候也會像国王一樣將其公開來宣傳力量就是了。

「是的呢，謝謝你。」

在對話中，肉類已經全部交易完了，剩下的是要賣給錬金術師的部分。
順帶一提，大鬼的肉其實不便宜，它是非常美味的，有點像野豬那樣。野豬也是種美味的食材，但大鬼肉附著的油脂更加美味，味道也更鮮美。它的價錢無可避免的更高了，因為它非常不易取得。我很期待大約４０公斤的肉價值多少錢。

「嗯…在解體方面也做的很好，我會用比市價更高的價錢買下它，因為可以順便作為成年禮的禮物。」

說著，老闆想了一下，給了我１枚金幣
和２３枚銀幣。
真是太好了，就算賣了一隻兔子，也只有３枚銅幣！順帶一提，十枚銅幣等於１枚銀幣，１００枚銀幣等於１枚金幣，１００枚金幣和１枚白金幣是等值的。直到昨天為止，我一天的收入大概是８枚銅幣～１枚銀幣之間。所以只要賣出一隻大鬼，便可以得到１２３天的收入。

「今天是特別的，是為了恭喜你成年才有這樣的價錢，記得，１公斤的大鬼肉大概是３枚銀幣。」
「是的，我明白了，謝謝你，叔叔」替代，拿來的是大鬼的素材。」

我說的話，讓他的笑容一瞬間痙攣了

「那個，是瑪因把大鬼打倒的嗎？」
「嗯，沒錯，靠著昨天被授予的技能所打倒的。」

和肉舖的叔叔一樣，也意識到我是個成年人了呢。

「那麼，這些就是素材了」

接著，拿出了屠夫沒拿走的內臟，以及放在玻璃瓶中的眼球、睪丸和魔石，放在了檯子上。魔石是藏在大鬼心臟的一粒小石頭，累積了許多魔力。它用於錬金術，也用於魔道具的燃料。也是這次最有價值的商品。

「嗯…是經過俐落的解體的呢，我會用更好的價格買下，大鬼骨呢？我也會買下大鬼骨的哦。」
「……我丟掉了……儲物袋空間不夠」

是的，儲物袋只能放下５０公斤的東西，而大鬼總重１００公斤，所以我把笨重或沒價值的東西都丟在解體的地方了。

「嗯…這是我父親所做的，好像只能放大約５０公斤吧？」

一邊說著，哥哥好像在考慮著什麼。

「這邊是買進的價格，內臟全部共８枚銀幣，睪丸２０枚銀幣，眼睛４枚銀幣，
魔石１枚金幣。」

看來被用更高的價格收購了，實在是感謝哥哥啊。

「順便問一下，瑪因，你現在有多少錢？」哥哥突然問道

「……從肉舖那邊，賣得了１枚金幣和２３枚銀幣。」

當我說完時，哥哥他又想了一下。

「那你現在有２枚金幣和５５枚銀幣嗎………好，瑪因你可以在狩獵３隻小鬼嗎？這樣的話，再加上兩枚金幣，便給你１０噸的儲物袋。」

從他的話中，我聽出了他的意圖。

因為一隻大鬼便有相當的強度，如果不是Ｃ級冒険者的話，要單獨狩獵是很危險的，但似乎Ｃ級冒険者所要賣的價格相當不便宜。而對Ｄ級冒険者而言，大鬼太過強大，需要很多人才能狩獵，在人數的稀釋下，每人所得到的利潤很低，所以沒人想去狩獵。但也因為沒人去狩獵大鬼是不行的，所以只能透過平時的討伐委託來取得大鬼，但數量相當稀少。素材中的睪丸，似乎成為了滋養品，許多貴族都想要它，所以需要不少大鬼。
而這時，一個可以單獨狩獵大鬼的新
人出現了，但由于儲物袋的容量不足，無法帶來令人滿意的數量，便宜的給了他一個足夠大的儲物袋，如果他能夠穩定的帶來大鬼，你也將受益的吧。而且如果以前便是熟人的話，合作是司空見慣的，還有就是，這也是慶祝我的成人。
在這種情況下，我決定聽從哥哥他的主意。

「明白了，我會把小鬼帶來的」

聽了我的回答而露出笑容後，哥哥他從店的後面拿出了一個黑色袋子。

「那麼，這就先給了你吧，如果有更
多也能帶來哦。」

因為從爸爸那代就開始交流了，所以被信任了吧，決定心懷感激的收下了。

「謝謝，會盡早拿來的。」

哥哥他，就像平常那樣笑著看向如此回覆的我。
────────────────────以上的叔叔和哥哥應該都沒有血緣關係吧？

－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00008話：第07話 オークよ、再び
第07話 オークよ、再び
－－－－－－－－－－－－－－－BODY

现在还是在上午，赶紧出去狩 猎吧。

我无论如何都想报答他（上一話的歐尼醬）相信我而先把收納袋交给我这件事。

豬人的聚居地意外的在森林深处，也有像剛才那家伙那样浮躁的單体会瞎Jier跑。

因为有了大的收納袋，所以如果能顺便猎 取兔子和羊的話就好了。

回了一次家，把昨天剩下的肉干啃了以后，再次向荒野（森林）进发。

早上的衛兵换班了吧，换成了不同的人。

这个衛兵我也混了个脸熟，送我出去的时候「小心一点啊」被这么提醒了，很开心。

途中发生的小事让我活力满满地走进了森林。

在向森林深处走去的途中，出现了几次兔子，都被我锤爆了。

干脆地打倒后，放入了收納袋。

打倒了三只兔子后，发现了新怪物。

森林爬虫，芋虫だ

名字：森林芋虫
種族：爬行动物─族
性別：无

【技能】

补助魔法・速度低下（可能是让對方迟缓）

嗯？持有魔法啊，这家伙。

快点夺走吧（又没人跟你抢）

那就赶快解決它吧

我肯定是轻敌……不對，应该說是太自大了吧。
想着打倒了豬人的我不可能会输给芋虫…

我站到了芋虫的正前方，将短 劍横在身前。

然后，使用【脚力強化・小】一口气缩短距离。

将短 劍举起起，然后发出咸魚突 刺。在我这么想的时候，那家伙从嘴里吐出了什么。

感覺到不妙并后退时已为时已晚，那家伙吐出的東西命中了我的右膝蓋。（直到我的膝蓋中了一劍）

「呜哇、！」

没有痛覺，虽然如此，我的膝蓋被白色的絲線缠绕住了。
而且那絲線有強力的粘性，把我黏在了地上，逃脱不得。

「啊、脚动不了」

我慌忙的在地上挣扎，***。

然后，就當我将注意力完全集中在自己身上的时候，那家伙已经到我跟前了。

「啊疼」

而且正准備用意外尖锐的牙齿啃咬我的手臂。（解決它吧，決它吧，它吧，吧）

「呀啊↑啊↓啊↑啊↓啊↑」

牙白，牙白，牙白（不妙），不做点什么的話。

我挥舞着短 劍，但是由于腿没有像想象中的那样动，所以全部挥空了。

在我挣扎的时候，那家伙又吐了那个白絲出来。

这次命中了身体。

动作以肉眼可見的速度变慢。

對哦，【魔法・風】的話应该…

我拼命发射着魔法，其中命中了几发，芋虫喷出了绿色的Xue。

然后趁它被Xue液蓋住視線的时候，发动【豪腕】将短 劍拼命刺出。

短 劍命中了那家伙的眉間，整把短劍都没入了进去。

那家伙缓缓倒向地面，看上去总算是打倒了。

「……得、得救了……」

我都在想些什么。

确實，我得到了好几个技能，比以前更強了。

不过，终究是初出茅庐的猎 人罢了。

能打倒豬人也是因为慎重的戰斗了。

骄傲自大，隨意应付戰斗是不会胜利的。

因为是芋虫，所以这種程度足够了，但如果是豬人，在这種情况下……

需要深刻反省，然后把自己是半吊子这件事铭记在心。

否则我会早早狗带的吧。

與芋虫的这一戰给了我一个很好的教训。

在心中自我反省之后，审視了周边的状况。

被芋虫的Xue液溅了一身，實在是有点惡心。

整只右腿还缠绕着白色絲線。

总之，不想办法弄掉这白色絲線的話，也没办法行动。

用【剪切】把線切断。（你剛才怎么没想着用）

这線說不定能賣錢，拿着回去吧。

这么想着，放进了父亲遺留下来的收納袋。

小物品就放进这个袋子，大的就放进那个新的。

然后拿出藥水，涂抹到被芋虫咬伤的伤口上。

藥水冒出烟雾，伤口很快凝結了。

②以防万一带上的藥水起了大作用。
我带着的是最便宜的藥水，即便如此也有一定的價格。
對于勉強維持生活的我，这确實是比较好的选择。
剩下来的錢要不要去錬金術屋再買啊……
嗯？錬金術？我好像有来着？

【錬金術】：把各種各样的東西調和在一起，做出新的東西，熟练度越高做出来的東西效果越好。
哦哦，正是錬金術！
难道可以做出藥水吗！？

【下級藥水】：效果较弱的藥水。用藥草和干净的水調和而成。
說到干净的水，普通的水就做不了吗……
总之试试采集藥草吧。
去看看錬金術屋的哥哥或许也不错。
O**K，之后就来解体芋虫吧、啊。
我不知道哪些是可以賣得出的，就这样全带走吧。
这个就可以放进小收納袋。
将芋虫放入后，再次向森林深处进发。
走了一段时間后，有几只兔子跳出来，被我麻利地打倒了。
再往里面走，就发现了目标的羊。
名字：森林羊
種族：大羊族
性別：母

【技能】

补助魔法・睡眠

【能力】

突撃
像想象中的那样持有技能啊，當然要抢过来。
把能力切下来，扔掉。
よし，果決的戰斗吧。
我轉向它的背后，赏了它一发必殺技的【魔法・風】。

不用說，魔法命中了，羊Xue一瞬間爆出，我接近不知道发生什么陷入狂暴的羊，用【豪腕】驱使短劍一口气将羊撕裂开来。
羊僵硬的倒在地上，胜负已決。（老Yinbi）

大概，这只羊的生命被莫名其妙的夺走了吧。
在別的怪物来之前把它解体，用小袋子装着。
差不多，收获颇丰。今天早上就是到这里回去的，袋子还有空間。
还没有打倒關键的豬人，當然还要前进。
不用多說，不要犹豫慎重一些！
那之后，在途中打倒了几匹羊啊兔子啊之类的，然后发现了豬人。
而且有三頭。
一頭应该没有問题，但是奇袭只能袭击一頭吧。
要解決这个問题，应该先用先前入手的两个辅助魔法吧。

【补助魔法・速度低下】【补助魔法・睡眠】だ。
先让一頭睡着，如果没有察覺的話，就让另一頭也睡着。
稍微有些距离，所以让【补助魔法・速度低下】和【魔法・風】叠加了。
然后就按流程来吧。
一开始就让两頭睡着的話，之后就会轻松不少吧。
首先预備好【豪腕】【脚力強化・小】【視力強化・中】

不用說，不会忘了事先把技能和能力夺取的。
稍微拉开距离，然后从后方看准时机…就是现在！【辅助魔法・睡眠】！！

好，看起来是睡着了。再来一次！
切，不行吗，被注意到了所以被挡住了。
计划变更，向剩下的两頭发射【辅助魔法・速度低下】

它们速度变慢了以后，将短 劍奋力投出。
短 劍完美的切裂了它的右臂。豬人大声的叫着。

③然后，對着另一頭发射風魔法。
豬人也没想到那里会有魔法射出，所以被直接糊脸上了。
这样两頭都被打伤了，动作也因为愤怒而变得單調，或许是因为受到伤害的緣故吧，动作也变差了。
被糊脸的放在后面打也没關系，先打倒那只被砍 手的那只。
这么想着的时候，背后突然传来巨大的冲击力。

「ぐぉっ」

看准了的豬人好像一拳打在了我的背上。
虽然脚下摇摇晃晃，但我还是站了起来，拉开距离。
再向那家伙的脚下发射風魔法，瞄准它害怕的瞬間再次拉近距离。
然后全力挥出短 劍，把它割 喉了。
他发出了临 终前的惨叫，倒在地上翻白眼，身体还在瞎乱挥舞着石斧。
到了另一頭，因为眼睛看不見所以没必要接近，在稍远的地方发射了風魔法，终止了它的呼吸。
这样就只剩下睡着的那一頭了。
突然醒来感到很困惑，我再度拉近距离发射【辅助魔法・睡眠】

确认了它已经不再动了以后，从背后心 脏的位置刺进了短 劍。
虽然陷入了苦戰，但总算是成功打倒了三頭豬人的我當場瘫软在了地上。

－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00009話：第08話 錬金術と謎の男達
第08話 錬金術と謎の男達
－－－－－－－－－－－－－－－BODY

雖說是苦戰，但只靠今天就能打倒三只目標的豬頭人是個收穫。
還以為最快也需要兩～三天的時間。

這麼一來，這個收納袋就成了我的東西了。
在這次的狩獵中，這個收納袋的有用性已經深切地理解了。

本來自己是知道的，但實際使用起來的話可以實際體會出來。

雖說已經解體了，但還是收納了三只豬頭人，而且還很充裕。

在回家的路上，即使狩獵到的草藥類和其他獵物也不用丟掉，這在精神上和手頭所持有的金錢上都是非常可貴的。

一邊思考著這種事情，一邊與芋蟲的一戰中所得到的教訓小心翼翼地回家去。
不忘使用鑑定，將藥草類一一收納起來。

結果，在離開森林之前又打倒了一只芋蟲，還有兩只森林羊。

穿過後門的時候，看到被芋蟲的體液染成綠色的我，讓熟悉的門衛桑驚慌失措的，實在抱歉。

但是這副樣子，去肉店桑和錬金術屋桑，真是讓人不好意思啊。

這麼考慮的我決定先回到自己家。

剛一到家，就脫下髒衣服，從井裡多打些水，從頭上撲的潑了點水。
在汲取的水沒了的時候，總算能把綠色也沖完。

用乾燥的布擦掉裝備的水之後，放在房子裡晾乾，並且換了一套新衣服。

染成綠色的衣服……怎樣洗也洗不乾净嗎………
總之，暫且先放在水里放著不管吧。

完成這項工作後，太陽也沉下去了。

我急急忙忙地去蔬菜店桑，買幾個用於晚飯用的蔬菜。
然後就這樣扔進收納袋裡，急急忙忙趕往錬金術屋桑那。

順便說一下，因為在城中所以當然沒有使用技能。
很有可能成為掌握別人技能的行為，所以要特別注意小心使用。

「晚上好～！」

剛一到錬金術屋桑，我便對著店內大聲喊道。
然後「啊啦啊啦，嘛嘛（まあまあ，來了來了）」夫人出來了。

「從我丈夫那裡聽說了喲，你打倒了豬頭人？馬恩醬，好厲害啊！歐妮桑（姐姐）我大吃一驚啊♪」

被美人的夫人誇獎的不得了，不由得臉都紅了。

「誒嘿嘿，還不到那種程度吧？話說回來，歐尼桑（哥哥）在嗎？」

「是是，我現在就去叫他啊」

說完，夫人消失在店裡了。
等了一會兒，歐尼桑從裡面邊用毛巾擦手，邊出來了。

「馬恩君，怎麼了嗎？」

「我打倒了，三只豬頭人！」

當我精神飽滿地回答歐尼桑的問題時，歐尼桑的臉和早上看到的一樣，表情很僵硬。

「……是不是有點早？」

「我想是運氣不錯！雖說有三只豬頭人，但是要肉嗎？」

「運氣什麼的……運氣好的話也不可能打倒三只豬頭人的」

歐尼桑露出苦笑，因為說不要肉，所以只要求拿出平時的素材就行了。

說到底有三只豬頭人，因為有骨頭，所以我們移動到店鋪深處那寬敞的作業場，在那裡取出了所有的東西。

「嗯，確實是三只。早上的時候也這樣想，但確實是順利成功地解體了」

繼夫人之後，在歐尼桑的誇獎下，臉上再度有點熱熱的。

「素材完全沒有問題，正如約定的那樣那個收納袋就馬恩君，這就歸你的了」

雖然沒有想到信賴的哥哥竟然會說謊，但是還是被這樣重新明確地說出來了，讓我鬆了一口氣。

「謝謝你！如果再去狩獵的話，我再給你拿來的！啊，對了，這個也能不能買下來呢？」

說完之後，把已經解體的芋蟲的素材和那傢伙吐出的絲線都拿了出來。
啊，羊角和蹄子能賣出去嗎？

「嘿～，是芋蟲啊。肉不管是我們店還是肉店都可以買進吧。觸覺和尾巴，還有這根絲線是可以買的」

說著，就給了我４枚銀幣和６枚銅幣。
具體為兩只觸覺和尾巴是３枚銀幣，還有絲線總共是１枚銀幣和６枚銅幣。

出乎意料的是，絲線的價錢似乎很高。
據說可以成為昂貴的布料什麼的。

「還有６只羊角和蹄子共２４枚銀幣」

這個好像也意外地買得很貴。
順便說一下，聽說這個角和蹄子磨成粉末，和什麼東西一起配的話會成為安眠藥。

面對意外的收入，我欣喜若狂，我決定試著問一下在狩獵過程中想到的事情。

「……話說我有事想拜託你一下……」

「請求？你突然怎麼了？改了樣似的」

「……如果可以的話，我想參觀一下調劑藥水（Potion）但是……不行嗎？」

大概是因為我的提議出乎意料之外吧，歐尼桑一邊露出啞然的表情，一邊稍微思考了一下。

「是沒關係哦，但是……看了如果沒什麼技能的話，怎麼也做也做不好哦？。如果是質量不好的東西的話，反复練習的話說不定能做出來呢……」

「欸，不要緊！拜託你了！！」


◆◇◆◇◆◇◆◇◆◇◆◇

總而言之，可以用井水和藥草來製作藥水。
流向盧卡斯城鎮的地下水似乎是「淨水」

井水的話，可以汲取多少就汲取多少。
趕緊回家後做做看吧。

順便說一下，我也參觀了高級藥水（High Potion）的調劑。

配方好像是用藥草＋」純流水」的水來製作。

據說這是在從盧卡斯城鎮坐馬車兩日左右到穆伊爾山流淌的清流上游所採集的水。
當然，應該還有其他可以獲得」純流水」的地方吧，不過那裡是離這個城市最近的。

調劑已經告一段落了，向兩人致謝，準備經過肉店桑前往自己的家。

肉店桑也差不多該關門了，能不能來得及這點有些微妙。

拐過這個街角就是肉店桑了。
不由得加快了步伐。

轉過街角時，映入眼簾的是關著店門的歐吉桑的身影。

「哦？是小子啊，你怎麼這麼慌慌張張的？」

看到我身影的歐吉桑，牙齒一閃地向我打招呼。
自己非常喜歡那個，明明長著一副嚴厲的面孔卻完全讓人不害怕的笑臉！

「太好了，歐吉桑……還能買進嗎？」

「嗯？又狩獵到什麼了嗎？可以哦，不過先讓我把店關上吧。不然客人就會沒完沒了的」

歐吉桑一邊嘎哈哈地笑著，一邊重新開始整理。

「我也來幫忙吧！」

歐吉桑說「哦可以嗎！那能拜託你嗎？」一邊說著一邊把掃帚遞給了我。

兩人開始收拾５分鐘左右，最後順利地完成了整理。

就這樣立刻移動到關了門的店內，在歐吉桑的催促下把豬頭人的肉，還有把兔肉、羊肉都拿出來了。

「……小子，豬頭人又狩獵過來了嗎？按這個分量來看……兩只，不對是三只嗎？你已經足夠可以獨立了」

這樣說著，歐吉桑給了我５枚金幣和５５枚銀幣。

「稍等我把零數給弄好了，一口氣變得很有錢了小子！嘎哈哈哈」

把錢放進收納袋裡，向歐吉桑道謝。

「一直以來謝謝你，歐吉桑！」

「沒關係對我也是好事，你明天打算再去狩獵嗎？這樣的話，你可要小心啊？」

歐吉桑擔心地告訴我……真是感激不盡啊。

「嗯！謝謝，我會一邊回想起歐吉桑的忠告一邊狩獵的！那麼，晚安了！」

和他說聲謝謝，這次一定要回家了。

我今天好像能做個好夢。


◆◇◆◇◆◇◆◇◆◇◆◇

「今天，有個人獲得了很有趣的技能」

「……你特地來報告看來是相當了不起的，是怎樣的技能啊？」

「是【鑑定・全】」

「吼」

在黑暗之中，有兩個男人在說話。

這兩人的對話後來會給馬恩的人生帶來巨大的影響，但現在馬恩本人當然不知道啦。

－－－－－－－－－－－－－－－END


－－－－－－－－－－－－－－－BEGIN
第00010話：第09話 冒険者公會的約束
第09話 冒険者公會的約束
－－－－－－－－－－－－－－－BODY

新譯名：
ノブチュ───諾布丘（好像是植物）

オーガスタ───奧加斯塔王国

────────

「但是、真的能用井水來做藥水啊…」

我在回到家後馬上試做藥水。

正如預料的那樣、錬金術桑在看見用流水做出來的藥水後的第一句話。

用採集的藥草攸了大約６升的藥水。
但果然把這個賣出去的話、會妨礙到錬金術桑的生意吧…

（畢竟）被教了不能恩將仇報呢。
自己用多少就做多少吧。

「有了這些的話、就能撐一會兒了吧？」

做出來的藥水裝在了以前在雜貨店買的大透明瓶子裡。
明天、買點小瓶子來分開吧。

工作大概完成了後稍微喘口氣、肚子發出了咕～～～的聲音。

「這麼說來、還沒吃飯呢」

自己吃剩下的鐵板燒豬人肉、再加上在蔬菜店桑那裡買回來的諾布丘的葉做成沙拉。

多虧了料理技能，雖然是很簡單的菜、但還是很美味。
話雖如此、因為吃的是比平常吃的兔肉高級很多的豬人肉、所以也不全是技能的功勞。

雖然做了挺多的份量、但還是全部吃光了、慢慢的變得愜意起來。
身體自然的脫力了、啪達的倒在床上。

「今天也平安無事、幸存下來了…突然就忙起來了。嘛、不過收入也變得很厲害就是了…」

今天得到了金幣５枚、銀幣８３枚、還有６枚銅幣。
不只是錢、還得到了收納袋（特大）。

全部、都是多虧了賜予我的技能、對神大人的感謝怎樣都不夠啊。

考慮到收入、以及技能的事、果然還是註冊成為冒険者吧。
明天、就去冒険者公會露一下臉吧。

認真的考慮了一下。

好！今天也很努力了！晚安！


◆◇◆◇◆◇◆◇◆◇◆◇

『冒険者公會』

那是不只在馬恩所在的盧卡斯鎮所屬的奧加斯塔王国、而且包括他国在內的大部份城市都有的營利團體。

（冒険者公會的）起源是在數百年前出現的英雄、為了更有效率的討伐魔物而把人聚集起來傳授戰鬥向技能的團體。

那組織經過時代的變遷後、變得越發成熟。

為了提高與魔物們戰鬥而受傷倒下的戰士和冒険者們的生存率、構築了情報網絡、並以此營利。
而那個支援他們生活的組織現在改變了。

在各大陸、国家、城市等地設置了支部、為每天登記的冒険者提供支援。

冒険者公會基本上是早午晚全年無休的、而且深夜也照常營業。
為了在發生突發事件時能立刻應對。

雖然是營利組織、擁有獨立的經營基礎、但也基本上擔任著所在的城市的防衛。

對外宣稱是接受了国王、領主、以及設置地方的首長的防衛委託。
當然、既然是委託的話也會有報酬的。

在數年出現一次的魔物大進攻<<氾濫>>之類的毀滅性災禍發生時也會有正當的報酬。

而如果沒有什麼特別的事態的話国家和城鎮等就會每月支付一定的金額、作為冒険者完全委託時的報酬的一部份。

而現在有一位少年、不，已成年的青年來到了這樣的冒険者公會中。
沒錯、他就是馬恩。


◆◇◆◇◆◇◆◇◆◇◆◇

「這裡就是公會啊、還是第一次進來呢…」

不知不覺的東張西望了起來。

然後、發現了前幾天從王都回來時一起乘馬車的Ｃ級冒険者基斯先生。
還看到了那時的其他冒険者桑們。

基斯先生他們站在像是公告板一樣的大板子前抱着臂、認真的看着貼在板子上的紙。

『很認真的表情呢、在看什麼呢…』

雖然很在意他在看什麼、但還是先去接待處問一下各種問題吧。
要成為冒険者的話、情報是十分重要的。

排在幾個櫃台中最少人排隊的、等着輪到自己。
然後、輪到我的時候、我向漂亮的接待姐姐打了聲招呼。

「早上好！」

大姐姐被我的問候嚇到、一瞬間呆住了、然後馬上笑着對我說【早上好】。

「那個、我叫馬恩。其實我在昨天迎來了成人禮…然後我對冒険者有點興趣，但是有些不明白的地方，所以今天來向你請教」

聽到那句話後、大姐姐又露出了驚訝的表情。
啊咧？是不能來提問的嗎？

「…不好意思、給你添麻煩了嗎」

（看到）沒精打采的我、大姐姐慌張的搖着頭說【唔、沒有那回事】。

「…真的嗎？你不介意嗎？」

一想到她可能只是顧慮到我的感受、我再次垂頭喪氣的問。

「真的沒有那一回事、不好意思了呢。畢竟你看、冒険者們中大多數都是莽夫。而且馬恩君你很有禮貌所以嚇了我一跳」

雖然妳對我說了一堆，但我可是連這些事都不知道就來了啊…這樣想着的同時心裡安心了。

「太好了、那我就放心了」

「那麼、你想問什麼呢？」

因為我事先已經整理好想要打聽的東西、所以我一項一項的問下去。

然後我知道的事有

────────────────────

・在公會登錄後會發行公會卡、可以當作身份證明書使用。
・公會是向原則上登錄了的冒険者以委託的方式介紹工作的地方。
・冒険者是有分等級的、完成了委託的話等級就會逐漸上升、上升後就能接受報酬更好的委託。
・委託有分為【討伐】「採集」【護衛】「特殊」等。要提升等級的話就要接受過各種的委託。
・在一定等級以上的冒険者有義務接受有關城鎮存亡的【緊急委託】

・公會會收購在委託中、或是委託之外所得到的素材
・公會的一員不需要交稅。因為委託報酬的一部份已經事先作為稅金扣除了。
・長期不接、或是不接受委託的話會被公會除名、而且會被要求支付作為公會一員的時候沒支付的稅金。

────────────────────

大概就是這樣。

根據接受了的委託、有必要去想像以外遠的城鎮或迷宮之類的。

嗯、對我來說好處多多。
只是、因為想要隱藏自己的技能、所以組成隊伍接受任務有點困難吧。

「感謝您在百忙之中告訴我這些事情。聽了這些話後、我決定成為冒険者了。」

在我道謝時大姐姐笑着說【如果能成為你的參考的話、我會很高興的】。

美人元氣的笑了的話我也忍不住笑了。

「那麼、你願意成為冒険者嗎？」

「切、小鬼要做冒険者嗎？別小看冒険者啊」

唔、好像要發生什麼麻煩事了。

－－－－－－－－－－－－－－－END

